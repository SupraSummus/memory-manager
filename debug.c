#include "debug.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>

const bool TODO = true;
const bool WARN = true;
const bool ERRO = true;
const bool INFO = true;
const bool SPAM = true;

/* colors */
#define KNRM  "\x1B[0m"
#define KDIM  "\x1B[2m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

void todo(const char* format, ...){
	if(TODO){
		va_list l;
		
		fprintf(stderr, "["KCYN"TODO"RESET"] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void warn(const char* format, ...){
	if(WARN){
		va_list l;
		
		fprintf(stderr, "["KYEL"WARN"RESET"] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void erro(const char* format, ...){
	if(ERRO){
		va_list l;
		
		fprintf(stderr, "["KRED"ERRO"RESET"] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void info(const char* format, ...){
	if(INFO){
		va_list l;
		
		fprintf(stderr, "[INFO] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n");
	}
}

void spam(const char* format, ...){
	if(SPAM){
		va_list l;
		
		fprintf(stderr, KDIM"[SPAM] ");
		
		va_start(l, format);
		vfprintf(stderr, format, l);
		va_end(l);
		
		fprintf(stderr, "\n"RESET);
	}
}
