#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H

#include <stddef.h>
#include <stdbool.h>

/**
 * Interface to memory manager
 * 
 * Memory manager holds chunks of cheap memory. Some chunks holds
 * pointers to other chunks. Chunk can be freed if no other chunk has
 * reference to it or there is a not accessible cycle of references.
 *
 * Sometimes pointer to chunk is stored in stack or other untraced
 * memory - then chunk can't be referenced from memory traced by
 * memory manager but shouldn't be freed. If you want to allocate such
 * chunk you should set parent to NULL (other untraced pointers will
 * cause warnings).
 */

typedef struct _MemoryManager MemoryManager;
typedef void (MemoryManagerFreeFunction)(MemoryManager*, void*);

/**
 * constructor, destructor
 */
extern MemoryManager* memory_manager_new();
extern void memory_manager_free(MemoryManager*);

/**
 * allocate new memory block of given size, needed by parent
 *
 * parent must be pointer traced by memory manager or NULL
 *
 * free function will be called before memory block is recycled
 * if free function is NULL, it won't be called
 * don't free memory block inside free function - it's done by garbage collector
 */
extern void* memory_manager_malloc(MemoryManager* memory_manager, size_t size, void const* parent, MemoryManagerFreeFunction*);

/**
 * child is now needed by parent (increase reference counter)
 * we allow multiple requirements
 *
 * child must be memory chunk traced by memory manager
 * parent must be memory chunk traced by memory manager or NULL (needed from outside of traced memory)
 */
extern void memory_manager_needed(MemoryManager* memory_manager, void const* child, void const* parent);

/**
 * child is not needed anymore by parent (decrease reference counter)
 *
 * child must be memory chunk traced by memory manager and needed by parent
 */
extern void memory_manager_not_needed(MemoryManager* memory_manager, void* child, void const* parent);

/**
 * force cycle detection
 */
extern void memory_manager_full_clean(MemoryManager* memory_manager);

extern bool memory_manager_is_traced(MemoryManager*, void const* block);
extern size_t memory_manager_get_size(MemoryManager*);

/**
 * dump dependencies data
 * for debugging reasons
 */
extern void memory_manager_print_debug(MemoryManager*);

#endif
