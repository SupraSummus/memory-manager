#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>
#include <stddef.h>

typedef struct _Queue Queue;

typedef void* (QueueMallocFunction)(size_t);
typedef void (QueueFreeFunction)(void*);

Queue* queue_new(QueueMallocFunction*, QueueFreeFunction*);
void queue_free();

void queue_push(Queue*, void*);

/**
 * if queue is emty returns null
 */
void* queue_get(Queue*);

bool queue_is_empty(Queue*);

#endif
