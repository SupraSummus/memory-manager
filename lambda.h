#ifndef LAMBDA_H
#define LAMBDA_H

/**
 * macro for creating anonymous function
 *
 * idea from:
 * http://stackoverflow.com/questions/10405436/anonymous-functions-using-gcc-statement-expressions
 *
 * example:
 *   int (*max)(int, int) = lambda (int, (int x, int y) { return x > y ? x : y; });
 *   max(4, 5);
 */

#define lambda(return_type, function_body) \
({ \
	return_type __fn__ function_body \
	__fn__; \
})

#endif
