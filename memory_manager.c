#include "memory_manager.h"
#include "hash_map/hash_map.h"
#include "hash_map/hash_map_utilities.h"
#include "lambda.h"
#include "debug.h"
#include "queue.h"
#include <assert.h>
#include <stdlib.h>
#include <execinfo.h> /* backtrace_symbols */

typedef size_t MemoryManagerReferenceCount;

typedef struct _MemoryManagerBlockData MemoryManagerBlockData;
struct _MemoryManagerBlockData {
	/**
	 * size of this block
	 */
	size_t size;
	
	/**
	 * key is pointer to child MemoryManagerBlockData (required by this block)
	 * value is pointer to number of references (MemoryManagerReferenceCount)
	 */
	HashMap* children;
	
	/**
	 * finalizer function
	 * NULL if no finalizer
	 */
	MemoryManagerFreeFunction* finalizer;
	
	/**
	 */
	enum {
		/**
		 * block is in use
		 */
		STATUS_ACCESSIBLE,
		
		/**
		 * block is not accessible - used during cycle detection
		 */
		STATUS_NOT_ACCESSIBLE,
		
		/**
		 * block is set to be deleted
		 */
		STATUS_WILL_BE_FREED,
	} status;
	
	/**
	 * number of blocks needing this block
	 */
	MemoryManagerReferenceCount parent_count;
	
	/**
	 * next block to be freed after this block
	 */
	MemoryManagerBlockData* next_to_be_freed;
	
	/**
	 * block itself
	 */
	char data[];
};

struct _MemoryManager {
	/**
	 * blocks is a map:
	 *  * key is pointer to MemoryManagerBlockData allocated by memory manager
	 *  * value is irrelevant (map acting like set) // TODO use real set
	 */
	HashMap* blocks;
	
	/**
	 * root block - all external references are represented using this block
	 */
	MemoryManagerBlockData* root_block;
	
	/**
	 * total size of allocated blocks
	 */
	size_t size;
	
	/**
	 * when total size reach this size full clen will be performed
	 */
	size_t full_clean_size;
	
	/**
	 * free queue data
	 */
	MemoryManagerBlockData* first_to_be_freed;
	MemoryManagerBlockData* last_to_be_freed;
};

/**
 * manage dependencies
 */
static void _register_link(MemoryManager*, MemoryManagerBlockData* child, MemoryManagerBlockData const* parent);
static void _unregister_link(MemoryManager*, MemoryManagerBlockData* child, MemoryManagerBlockData const* parent);

/**
 * decrease parent count of given block and free if block isn't needed by anyone
 */
static void _decrease_parent_count(MemoryManager*, MemoryManagerBlockData*);

/**
 * perform cycle detection and free detected cycles
 */
static void _full_clean(MemoryManager*);
static HashMapForEveryFunction _mark_not_accessible_callback;
static HashMapForEveryFunction _add_to_queue_callback;
static HashMapForEveryFunction _finalize_not_accessible_callback;
static HashMapForEveryFunction _remove_links_of_not_accessible_callback;

/**
 * run finalizer for given block
 */
static void _finalize(MemoryManager*, MemoryManagerBlockData*);

/**
 * remove all children links
 */
static void _remove_all_links(MemoryManager* memory_manager, MemoryManagerBlockData* block);
static HashMapForEveryFunction _free_link_and_decrease_parent_count_callback;

/**
 * free structures of block
 */
static void _free_block(MemoryManager*, MemoryManagerBlockData*);

/**
 * queue containing blocks to be freed
 */
static void _add_to_free_queue(MemoryManager*, MemoryManagerBlockData*);
static void _process_free_queue(MemoryManager*);

/**
 * compute MemoryManagerBlock* given pointer to it's data field
 */
static MemoryManagerBlockData* _block_from_pointer(MemoryManagerBlockData* root_block, void const* ptr);

/**
 * private functions implementation
 * ================================
 */

static void _register_link(MemoryManager* memory_manager, MemoryManagerBlockData* child_block, MemoryManagerBlockData const* parent_block) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, child_block)); /* child is traced by mm */
	assert(hash_map_has(memory_manager->blocks, parent_block)); /* parent is traced by mm */
	assert(child_block->parent_count > 0); /* detect use-after-free */
	assert(child_block->status == STATUS_ACCESSIBLE);
	assert(parent_block->children != NULL);
	assert(parent_block->status == STATUS_ACCESSIBLE);
	
	HashMap* parent_block_children = parent_block->children;
	
	MemoryManagerReferenceCount* reference_count = hash_map_get(parent_block_children, child_block);
	if (reference_count == NULL) {
		/* first link parent-child */
		reference_count = malloc(sizeof(MemoryManagerReferenceCount));
		*reference_count = 0;
		
		hash_map_set(parent_block_children, child_block, reference_count);
		
		(child_block->parent_count) ++;
	}
	(*reference_count) ++;
}

static void _unregister_link(MemoryManager* memory_manager, MemoryManagerBlockData* child_block, MemoryManagerBlockData const* parent_block) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, child_block)); /* child is traced by mm */
	assert(hash_map_has(memory_manager->blocks, parent_block)); /* parent is traced by mm */
	assert(child_block->status == STATUS_ACCESSIBLE);
	assert(parent_block->children != NULL);
	assert(parent_block->status == STATUS_ACCESSIBLE);
	
	HashMap* parent_block_children = parent_block->children;
	
	/* decrease reference count */
	MemoryManagerReferenceCount* reference_count = hash_map_get(parent_block_children, child_block);
	assert(reference_count != NULL); /* if reference_count is null then child isn't needed by parent */
	assert(*reference_count > 0);
	(*reference_count) --;
	
	if (*reference_count == 0) {
		/* child is not needed by parent at all */
		hash_map_remove(parent_block_children, child_block);
		free(reference_count);
		_decrease_parent_count(memory_manager, child_block);
	}
}

static void _decrease_parent_count(MemoryManager* memory_manager, MemoryManagerBlockData* block) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, block)); /* block is traced */
	assert(block->parent_count > 0); /* this block is needed by someone */
	
	block->parent_count --;
	if (block->parent_count == 0) {
		_add_to_free_queue(memory_manager, block);
	}
}

static void _full_clean(MemoryManager* memory_manager) {
	assert(memory_manager != NULL);
	/* check what can be accessed from root block - just a BFS */
	
	/* mark all as not-visited */
	hash_map_for_every(memory_manager->blocks, _mark_not_accessible_callback, NULL);
	
	/* queue contains MemoryManagerBlockData* */
	Queue* to_be_visited = queue_new(malloc, free);
	/* total size of accessible memory */
	size_t total_size = 0;
	/* begin with root block */
	queue_push(to_be_visited, memory_manager->root_block);
	
	while (!queue_is_empty(to_be_visited)) {
		MemoryManagerBlockData* block_data = queue_get(to_be_visited);
		if (block_data->status == STATUS_NOT_ACCESSIBLE) {
			block_data->status = STATUS_ACCESSIBLE;
			total_size += block_data->size;
			hash_map_for_every(block_data->children, _add_to_queue_callback, to_be_visited);
		}
	}
	queue_free(to_be_visited);
	
	/* run finalizers */
	hash_map_for_every(memory_manager->blocks, _finalize_not_accessible_callback, memory_manager);
	
	/* remove links */
	hash_map_for_every(memory_manager->blocks, _remove_links_of_not_accessible_callback, memory_manager);
	
	memory_manager->full_clean_size = total_size * 2 + 10;
}

static void _mark_not_accessible_callback(void const* void_block_data, void* irrelevant_value, void* null) {
	(void)irrelevant_value;
	(void)null;
	MemoryManagerBlockData* block_data = (void*)void_block_data;
	
	if (block_data->status == STATUS_ACCESSIBLE) {
		block_data->status = STATUS_NOT_ACCESSIBLE;
	}
}

static void _add_to_queue_callback(void const* void_block_data, void* irrelevant_value, void* void_queue) {
	(void)irrelevant_value;
	Queue* queue = void_queue;
	MemoryManagerBlockData* block_data = (void*)void_block_data;
	
	if (block_data->status == STATUS_NOT_ACCESSIBLE) {
		queue_push(queue, block_data);
	}
}

static void _finalize_not_accessible_callback(void const* void_block_data, void* irrelevant_value, void* void_memory_manager) {
	(void)irrelevant_value;
	MemoryManagerBlockData* block_data = (void*)void_block_data;
	MemoryManager* memory_manager = void_memory_manager;
	
	if (block_data->status == STATUS_NOT_ACCESSIBLE) {
		_finalize(memory_manager, block_data);
	}
}

static void _remove_links_of_not_accessible_callback(void const* void_block_data, void* irrelevant_value, void* void_memory_manager) {
	(void)irrelevant_value;
	MemoryManagerBlockData* block_data = (void*)void_block_data;
	MemoryManager* memory_manager = void_memory_manager;
	
	if (block_data->status == STATUS_NOT_ACCESSIBLE) {
		_remove_all_links(memory_manager, block_data);
	}
}

static void _finalize(MemoryManager* memory_manager, MemoryManagerBlockData* block_data) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, block_data)); /* block is traced */
	
	if (block_data->finalizer != NULL) {
		block_data->finalizer(memory_manager, block_data->data);
		/* set to null to prevent multiple runs */
		block_data->finalizer = NULL;
	}
}

static void _remove_all_links(MemoryManager* memory_manager, MemoryManagerBlockData* block) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, block)); /* block is traced */
	
	if (block->children != NULL) {
		/* remove links */
		hash_map_for_every(block->children, _free_link_and_decrease_parent_count_callback, memory_manager);
		hash_map_free(block->children);
		block->children = NULL;
	}
}

static void _free_link_and_decrease_parent_count_callback(void const* void_child, void* void_reference_count, void* void_memory_manager) {
	MemoryManagerBlockData* child = (void*)void_child;
	MemoryManagerReferenceCount* reference_count = void_reference_count;
	MemoryManager* memory_manager = void_memory_manager;
	
	_decrease_parent_count(memory_manager, child);
	free(reference_count);
}

static void _free_block(MemoryManager* memory_manager, MemoryManagerBlockData* block_data) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, block_data)); /* block is traced */
	
	/* unregister */
	hash_map_remove(memory_manager->blocks, block_data);
	assert(memory_manager->size >= block_data->size); /* prevent underrun */
	memory_manager->size -= block_data->size;
	
	/* free block data */
	hash_map_free(block_data->children); /* should be already null but to be sure ... */
	free(block_data);
}

static void _add_to_free_queue(MemoryManager* memory_manager, MemoryManagerBlockData* block) {
	assert(memory_manager != NULL);
	assert(hash_map_has(memory_manager->blocks, block)); /* block is traced */
	
	if (block->status == STATUS_WILL_BE_FREED) {
		/* already in queue */
		return;
	}
	
	if (memory_manager->first_to_be_freed == NULL) {
		memory_manager->first_to_be_freed = block;
	} else {
		memory_manager->last_to_be_freed->next_to_be_freed = block;
	}
	memory_manager->last_to_be_freed = block;
	block->next_to_be_freed = NULL;
	block->status = STATUS_WILL_BE_FREED;
}

static void _process_free_queue(MemoryManager* memory_manager) {
	assert(memory_manager != NULL);
	
	while (memory_manager->first_to_be_freed != NULL) {
		MemoryManagerBlockData* block = memory_manager->first_to_be_freed;
		memory_manager->first_to_be_freed = block->next_to_be_freed;
		if (memory_manager->first_to_be_freed == NULL) {
			memory_manager->last_to_be_freed = NULL;
		}
		
		_finalize(memory_manager, block);
		_remove_all_links(memory_manager, block);
		_free_block(memory_manager, block);
	}
}

static MemoryManagerBlockData* _block_from_pointer(MemoryManagerBlockData* root_block, void const* ptr) {
	if (ptr == NULL) {
		return root_block;
	} else {
		return (MemoryManagerBlockData*)((char*)ptr - offsetof(MemoryManagerBlockData, data));
	}
}

/**
 * public functions implementation
 * ===============================
 */

MemoryManager* memory_manager_new() {
	MemoryManager* memory_manager = malloc(sizeof(MemoryManager));
	memory_manager->blocks = pointer_key_hash_map_new();
	memory_manager->size = 0;
	memory_manager->full_clean_size = 10;
	memory_manager->first_to_be_freed = NULL;
	memory_manager->last_to_be_freed = NULL;
	
	/* create NULL block */
	MemoryManagerBlockData* null_block_data = malloc(sizeof(MemoryManagerBlockData));
	null_block_data->size = 0;
	null_block_data->children = pointer_key_hash_map_new();
	null_block_data->finalizer = NULL;
	null_block_data->status = STATUS_ACCESSIBLE;
	null_block_data->parent_count = 0;
	null_block_data->next_to_be_freed = NULL;
	
	memory_manager->root_block = null_block_data;
	hash_map_set(memory_manager->blocks, null_block_data, NULL);
	
	return memory_manager;
}

void memory_manager_free(MemoryManager* memory_manager) {
	memory_manager_full_clean(memory_manager);
	
	/* free null block data */
	hash_map_remove(memory_manager->blocks, memory_manager->root_block);
	hash_map_free(memory_manager->root_block->children);
	free(memory_manager->root_block);
	
	if (hash_map_elements_count(memory_manager->blocks) != 0) {
		warn(
			"memory_manager_free(): not all chunks was freed (%zu left), not freeing them - memory has leaked",
			hash_map_elements_count(memory_manager->blocks)
		);
	}
	
	hash_map_free(memory_manager->blocks);
	memory_manager->blocks = NULL;
	
	free(memory_manager);
}

void* memory_manager_malloc(MemoryManager* memory_manager, size_t size, void const* parent, MemoryManagerFreeFunction* free_function) {
	MemoryManagerBlockData* parent_block_data = _block_from_pointer(memory_manager->root_block, parent);
	assert(hash_map_has(memory_manager->blocks, parent_block_data)); /* block is traced */
	
	HashMap* parent_block_children = parent_block_data->children;
	
	MemoryManagerBlockData* child_block_data = malloc(sizeof(MemoryManagerBlockData) + size);
	hash_map_set(memory_manager->blocks, child_block_data, NULL);
	child_block_data->size = size;
	child_block_data->children = pointer_key_hash_map_new();
	child_block_data->finalizer = free_function;
	child_block_data->status = STATUS_ACCESSIBLE;
	child_block_data->parent_count = 1;
	child_block_data->next_to_be_freed = NULL;
	
	MemoryManagerReferenceCount* reference_count = malloc(sizeof(MemoryManagerReferenceCount));
	hash_map_set(parent_block_children, child_block_data, reference_count);
	*reference_count = 1;
	
	memory_manager->size += child_block_data->size;
	if (memory_manager->size >= memory_manager->full_clean_size) {
		memory_manager_full_clean(memory_manager);
	}
	
	return (char*)child_block_data + offsetof(MemoryManagerBlockData, data);
}

void memory_manager_needed(MemoryManager* memory_manager, void const* child, void const* parent) {
	if (_block_from_pointer(memory_manager->root_block, child) == memory_manager->root_block) {
		warn("memory_manager_needed(): child is NULL (parent %p)", parent);
		return;
	}
	
	_register_link(
		memory_manager,
		_block_from_pointer(memory_manager->root_block, child),
		_block_from_pointer(memory_manager->root_block, parent)
	);
}

void memory_manager_not_needed(MemoryManager* memory_manager, void* child, void const* parent) {
	if (_block_from_pointer(memory_manager->root_block, child) == memory_manager->root_block) {
		warn("memory_manager_not_needed(): child is NULL (parent %p)", parent);
		return;
	}
	
	_unregister_link(
		memory_manager,
		_block_from_pointer(memory_manager->root_block, child),
		_block_from_pointer(memory_manager->root_block, parent)
	);
	_process_free_queue(memory_manager);
}

void memory_manager_full_clean(MemoryManager* memory_manager) {
	_full_clean(memory_manager);
	_process_free_queue(memory_manager);
}

bool memory_manager_is_traced(MemoryManager* memory_manager, void const* block) {
	return (
		_block_from_pointer(memory_manager->root_block, block) != memory_manager->root_block && /* not a root block and ... */
		hash_map_has(memory_manager->blocks, _block_from_pointer(memory_manager->root_block, block)) /* ... stored in hashmap */
	);
}

size_t memory_manager_get_size(MemoryManager* memory_manager) {
	return memory_manager->size;
}

#pragma GCC diagnostic ignored "-Wpedantic"
// TODO thats awful - change compiler C standard?
void memory_manager_print_debug(MemoryManager* memory_manager) {
	info("##### chunk dependencies inside memory manager %p #####", memory_manager);
	
	size_t total_size = 0;
	
	/* root */
	MemoryManagerBlockData* null_block_data = memory_manager->root_block;
	if (!hash_map_has(memory_manager->blocks, null_block_data)) {
		warn("NULL block missing");
	} else {
		info("NULL block at %p", null_block_data);
	}
	
	/* check blocks */
	hash_map_for_every(
		memory_manager->blocks,
		lambda(void, (void const* void_block_data, void* irrelevant, void* null) {
			(void)null;
			(void)irrelevant;
			MemoryManagerBlockData const* block_data = void_block_data;
			
			void* ptr_array[1];
			ptr_array[0] = (void*)(block_data->finalizer);
			char** symbol_array = backtrace_symbols(ptr_array, 1);
			info("\tchunk %p of size %zu (finalizer %s)", block_data, block_data->size, symbol_array[0]);
			free(symbol_array);
			
			total_size += block_data->size;
			
			info("\t\tparent count %zu", block_data->parent_count);
			
			info("\t\tneeds:");
			hash_map_for_every(
				block_data->children,
				lambda(void, (void const* child, void* void_reference_count, void* null) {
					(void)null;
					MemoryManagerReferenceCount* reference_count = void_reference_count;
					
					if (reference_count == NULL || *reference_count == 0) {
						info("\t\t\t%p", child);
						warn("malformed reference count (NULL or 0)");
					} else {
						info("\t\t\t%p, \t%zu times", child, *reference_count);
					}
					
					if (!hash_map_has(memory_manager->blocks, child)) {
						warn("child not traced");
					}
					
				}),
				NULL
			);
			
		}),
		NULL
	);
	
	info("total blocks size %zu, full clean size %zu", memory_manager->size, memory_manager->full_clean_size);
	if (total_size != memory_manager->size) {
		warn("total size mismatched: sum is %zu but memory_manager->size is %zu", total_size, memory_manager->size);
	}
	
	info("##### end of debug dump #####");
}
#pragma GCC diagnostic pop
