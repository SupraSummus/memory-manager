#include "memory_manager.h"
#include <assert.h>
#include <stdlib.h>

/**
 * Test if finalizer is called when chunk is freed.
 *
 * just_an_int is 0 at the beginning but finalizer will increase it, so
 * we can tell if finalizer was called.
 */

static void test_finalizer_finalizer(MemoryManager* mm, void* ptr) {
	(void)mm;
	int* int_ptr = *(int**)ptr;
	(*int_ptr) ++;
}

static void test_finalizer() {
	MemoryManager* mm = memory_manager_new();
	
	int just_an_int = 0;
	int** chunk = memory_manager_malloc(mm, sizeof(int*), NULL, test_finalizer_finalizer);
	*chunk = &just_an_int;
	memory_manager_not_needed(mm, chunk, NULL);
	
	assert(just_an_int == 1);
	
	memory_manager_free(mm);
}

/**
 * Test if cyclic dependencies are freed
 */

static void test_cyclic_dependencies() {
	MemoryManager* mm = memory_manager_new();
	
	void* a = memory_manager_malloc(mm, 1, NULL, NULL);
	void* b = memory_manager_malloc(mm, 1, a, NULL);
	memory_manager_needed(mm, a, b);
	
	memory_manager_malloc(mm, 1, b, NULL);
	
	void* d = memory_manager_malloc(mm, 1, NULL, NULL);
	memory_manager_needed(mm, d, b);
	
	memory_manager_not_needed(mm, a, NULL);
	memory_manager_full_clean(mm);
	
	memory_manager_not_needed(mm, d, NULL);
	
	assert(memory_manager_get_size(mm) == 0);
	
	memory_manager_free(mm);
}

/**
 * Test if finalizer of blocks in cycle are called before blocks are freed
 *
 * To be sure this test works fine program must be run with memory error
 * detector (ie valgrind)
 */

typedef struct _TestCyclicDependeciesStruct TestCyclicDependeciesStruct;
struct _TestCyclicDependeciesStruct {
	int* number;
	TestCyclicDependeciesStruct* other;
};

static void test_cyclic_finalizers_finalizer(MemoryManager* mm, void* void_struct) {
	(void)mm;
	TestCyclicDependeciesStruct* s = void_struct;
	(*(s->other->number)) ++;
}

static void test_cyclic_finalizers() {
	MemoryManager* mm = memory_manager_new();
	
	int just_an_int = 0;
	TestCyclicDependeciesStruct* a = memory_manager_malloc(mm, sizeof(TestCyclicDependeciesStruct), NULL, test_cyclic_finalizers_finalizer);
	TestCyclicDependeciesStruct* b = memory_manager_malloc(mm, sizeof(TestCyclicDependeciesStruct), a, test_cyclic_finalizers_finalizer);
	memory_manager_needed(mm, a, b);
	a->number = &just_an_int;
	a->other = b;
	b->number = &just_an_int;
	b->other = a;
	
	memory_manager_not_needed(mm, a, NULL);
	memory_manager_full_clean(mm);
	
	assert(just_an_int == 2);
	
	memory_manager_free(mm);
}

/**
 * Test behviour of calling memory_manager_unneed inside finalizer
 */

static void test_unneed_in_finalizer_finalizer(MemoryManager* mm, void* void_ptr) {
	void** ptr = void_ptr;
	memory_manager_not_needed(mm, *ptr, NULL);
}

static void test_unneed_in_finalizer() {
	MemoryManager* mm = memory_manager_new();
	
	void** a = memory_manager_malloc(mm, sizeof(void*), NULL, test_unneed_in_finalizer_finalizer);
	void** b = memory_manager_malloc(mm, sizeof(void*), NULL, test_unneed_in_finalizer_finalizer);
	void** c = memory_manager_malloc(mm, 1, NULL, NULL);
	*a = b;
	*b = c;
	
	memory_manager_not_needed(mm, a, NULL);
	
	assert(memory_manager_get_size(mm) == 0);
	
	memory_manager_free(mm);
}

int main() {
	test_finalizer();
	test_cyclic_dependencies();
	test_cyclic_finalizers();
	test_unneed_in_finalizer();
	return EXIT_SUCCESS;
}
