build: `make`

file `memory_manager_all.o` is compiled library

api described in `memory_manager.h`

run tests: `make test`

tests require valgrind

to run tests without valgrind: `./memory_manager_test` but this won't test all possible bugs


