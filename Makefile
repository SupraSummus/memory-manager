CC = gcc
CFLAGS = -Wall -Wextra --pedantic -std=gnu99
LDFLAGS =
export

all: memory_manager.o memory_manager_all.o

test: memory_manager_test
	valgrind ./memory_manager_test

clean:
	make -C hash_map/ clean
	rm -rf *.o memory_manager_test

memory_manager_all.o: memory_manager.o queue.o debug.o
	make -C hash_map/ all
	ld -r $^ hash_map/hash_map.o hash_map/hash_map_utilities.o -o $@
	objcopy --wildcard --localize-symbol=!memory_manager_* --strip-unneeded $@

memory_manager_test: memory_manager_test.o memory_manager_all.o
	$(CC) $(LDFLAGS) -o $@ $^
